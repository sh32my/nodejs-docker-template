#!/bin/sh

## CONSTANTS ------------------------------>

# name of the application
APP_NAME=app

# current version of the application
APP_VERSION=0.1.0

# node version to use
NODE_VERSION=latest

## FUNCTIONS ------------------------------>

##
# Get current branch name from git.
#
# @return the branch name
#
get_branch() {
  echo $(git branch | grep \* | cut -d ' ' -f2);
}

##
# Get the tagname suffix from branch.
#
# @return nothing if production branch else -dev
#
get_branch_suffix() {
  branch=$(get_branch);

  # check branch name
  case "$branch" in

    # if master, release or hotfix return empty string
    ma* | re* | ho* ) echo;;

    # else it means it's a dev branch return -dev
    *) echo "-dev";;
  esac
}

##
# Get the full tag name of docker image.
#
# @param $1 the branch name
# @return the full tag name
#
get_tagname() {
  echo $APP_NAME:$APP_VERSION$1;
}

##
# Main entry point of script.
#
# @return void
#
main() {

  # process task requested
  process_task $(get_tagname)$(get_branch_suffix) $1;

  # if no errors (script still running), notify successful
  echo "\n** Build OK!\n";
}

##
# Check if error occurred, notify of error and exit script.
#
# @param $1 the error code
# @return void
#
process_error() {

  # if not successful, then notify and exit script
  if [ $? -gt 0 ]; then
    echo "\n** Build FAILED!\n";
    exit;
  fi
}

##
# Process the task requested.
#
# @param $1 the tagname of the application
# @param $2 the task requested
# @return void
#
process_task() {
  if [ "$2" = "build" ]; then
    task_build $1;
  elif [ "$2" = "clean" ]; then
    task_clean $1;
  elif [ "$2" = "help" ]; then
    task_help;
  elif [ "$2" = "name" ]; then
    task_name;
  elif [ "$2" = "run" ]; then
    task_run $1;
  elif [ "$2" = "test" ]; then
    task_run_test $1;
  elif [ "$2" = "version" ]; then
    task_version;
  else
    task_help;
  fi
}

##
# Build the docker image.
#
# @param $1 the tagname of the application
# @return void
#
task_build() {

  # clean build first
  task_clean $1;

  # build the docker image
  echo "\n** Building $1...\n";
  docker build --build-arg NODE_VERSION=$NODE_VERSION -t $1 -f Dockerfile .;

  # process any errors
  process_error $?;
}

##
# Build the docker test image.
#
# @param $1 the tag name of the application
# @return void
#
task_build_test() {

  # build application first
  task_build $1;

  # build the docker test image from application image
  echo "\n** Building $1 tests...\n";
  docker build --build-arg APP=$1 -t $1-test -f Dockerfile-test .;

  # process any errors
  process_error $?;
}

##
# Remove all build artifacts.
#
# @param $1 the tag name of docker image
# @return void
#
task_clean() {
  echo "\n** Cleaning $1...\n";

  # remove all associated docker images
  docker image rm -f $1; docker image rm -f $1-test;
  process_error $?;
}

##
# Outputs the help screen.
#
# @return void
#
task_help() {
  echo "\nUsage:   ./build.sh COMMAND\n";
  echo "Commands:";
  echo "  build      Clean and build the application";
  echo "  clean      Remove all build artifacts";
  echo "  help       Shows this screen";
  echo "  name       Outputs the application name";
  echo "  run        Clean, build and run the application";
  echo "  test       Clean, build and run the application and tests";
  echo "  version    Outputs the application version";
  echo;
  exit;
}

##
# Output the application name.
#
# @return void
#
task_name() {
  echo $APP_NAME;
  exit;
}

##
# Clean, build and run the application.
#
# @param $1 the tag name of docker image
# @return void
#
task_run() {

  # build application first
  task_build $1;

  # run docker image
  echo "\n** Running $1...\n";
  docker run --rm --tty $1;

  # process any errors
  process_error $?;
}

##
# Clean, build and run the application and tests.
#
# @param $1 the tag name of docker image
# @return void
#
task_run_test() {

  # build the test image first
  task_build_test $1;

  # run docker test image
  echo "\n** Running $1 tests...\n";
  docker run --rm --tty $1-test;

  #process any errors
  process_error $?;
}

##
# Output the application version.
#
# @return void
#
task_version() {
  echo $APP_VERSION;
  exit;
}

## ------------------------------>

# start the script
main $1;
