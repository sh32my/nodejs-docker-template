# nodejs-docker-template

This is a skeleton for nodejs application that uses generates the production docker image for deployment, and also the test docker image off of the production image. It allows you to quickly clean, build, test and run a node application that is to be deployed on docker.

The application files should be in the ```./app``` directory with the entry point as ```./app/index.js```.

The test files to be run should be in the ```./test``` directory with the entry point as ```./test/index.js```.

## Great, how do I get started?

To get started, take a copy of the repo and get it set up:

```shell
git clone https://gitlab.com/sh32my/nodejs-docker-template.git <new_project>
cd <new_project>
chmod +x build.sh
```

Then edit the build.sh file and set your app name, app version and preferred version of node.

## So.. what do I do with it now?

Simply run the build.sh script and you will be given a list of commands you can run:

```shell
Usage:   ./build.sh COMMAND

Commands:
  build      Clean and build the application
  clean      Remove all build artifacts
  help       Shows this screen
  name       Outputs the application name
  run        Clean, build and run the application
  test       Clean, build and run the application and tests
  version    Outputs the application version
```

## MIT Licence

Copyright 2019 Michael Edwards <me@sh32my.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
